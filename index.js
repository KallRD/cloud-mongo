//first NodeJS application


var mongoose = require('mongoose');
const express = require('express')
bodyParser = require('body-parser');
const app = express()
const port = 3000

app.use(bodyParser.json());

const MongoClient  = require('mongodb');
const connectionString = 'mongodb://192.168.1.106:40001,192.168.1.106:40002,192.168.1.106:40003/myCompany?replicaSet=cfgrs';

mongoose.connect(connectionString, {useNewUrlParser: true, useUnifiedTopology: true});

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

var Schema = mongoose.Schema;

var SomeModelSchema = new Schema({
  firstName: String,
  lastName: String
});

var SomeModel = mongoose.model('SomeModel', SomeModelSchema );



app.get('/', (req, res) => {
  SomeModel.find({},'firstName lastName', (err, staff) => {
    if(err) return handleError(err);
    res.send(JSON.stringify(staff))
  }) 
})

app.post('/',  (req, res) => {
  var awesome_instance = new SomeModel(req.body);
  awesome_instance.save(function (err) {
  if (err) res.send('Error');
    res.send(JSON.stringify(req.body))
  });
})

app.put('/',  (req, res) => {
  res.send('Got a PUT request at /')
})

app.delete('/',  (req, res) => {
  res.send('Got a DELETE request at /')
})


app.listen(port, () => {
 console.log(`Express Application listening at port 3000`)
})

